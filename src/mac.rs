use std::fmt;

use nom::be_u8;
// macros
use nom::{call, do_parse, named};

#[derive(Debug, Eq, PartialEq)]
pub struct OrgId {
    // Octets
    pub first: u8,
    pub second: u8,
    pub third: u8,
}

#[derive(Debug, Eq, PartialEq)]
pub struct NicId {
    // Octets
    pub fourth: u8,
    pub fifth: u8,
    pub sixth: u8,
}

#[derive(Eq, PartialEq)]
pub struct Mac {
    pub org_id: OrgId,
    pub nic_id: NicId,
}

impl fmt::Debug for Mac {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Mac {{ {:X}:{:X}:{:X}:{:X}:{:X}:{:X} }}",
            self.org_id.first,
            self.org_id.second,
            self.org_id.third,
            self.nic_id.fourth,
            self.nic_id.fifth,
            self.nic_id.sixth,
        )
    }
}

named!(
    pub org_id<OrgId>,
    do_parse!(
        first: be_u8
        >> second: be_u8
        >> third: be_u8
        >> (OrgId {
            first,
            second,
            third,
        })
    )
);

named!(
    pub nic_id<NicId>,
    do_parse!(
        fourth: be_u8
        >> fifth: be_u8
        >> sixth: be_u8
        >> (NicId {
            fourth,
            fifth,
            sixth,
        })
    )
);

named!(
    pub mac<Mac>,
    do_parse!(
        org_id: org_id
        >> nic_id: nic_id
        >> (Mac { org_id, nic_id }))
);
