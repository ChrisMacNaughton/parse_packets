use crate::Mac;

use crate::mac::mac;

use nom::be_u16;
// macros
use nom::{bits, bits_impl, call, cond, dbg_dmp, do_parse, named, take_bits, tuple, tuple_parser};

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_parses_a_basic_ethernet_frame() {
        let input = [
            // TODO: Identify chunks of unknown data in this packet
            // packet header
            0xac, 0x1f, 0x6b, 0x65, 0x66, 0x46, // destination MAC
            0x0c, 0xc4, 0x7a, 0xca, 0x57, 0xa6, // source MAC
            0x08, 0x00, // ether type
        ];

        let result = ethernet(&input).unwrap();
        println!("{:#?}", result);
        let frame = result.1;
        assert!(frame.vlan_header.is_none());
        assert_eq!(frame.ether_type, EthernetType::IPv4);
    }

    #[test]
    fn it_parses_a_vlan_ethernet_frame() {
        let input = [
            0xfa, 0x16, 0x3e, 0xa1, 0x0a, 0x20, // destination MAC
            0xfa, 0x16, 0x3e, 0xcb, 0xbe, 0x2b, // source MAC
            0x81, 0x00, 0x01, 0xf5, // vlan tags
            0x86, 0xdd, // ether type
        ];
        let result = ethernet(&input).unwrap();
        println!("{:#?}", result);
        let frame = result.1;
        assert_eq!(frame.src_mac.org_id.first, 250);
        assert_eq!(frame.src_mac.nic_id.fourth, 203);
        assert!(frame.vlan_header.is_some());
        assert_eq!(frame.vlan_header.unwrap().vlan_id, 501);
        assert_eq!(frame.ether_type, EthernetType::IPv6);
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum EthernetType {
    IPv4,
    IPv6,
}

impl EthernetType {
    pub fn from_u16(input: u16) -> EthernetType {
        match input {
            0x0800 => EthernetType::IPv4,
            0x86dd => EthernetType::IPv6,
            _ => {
                println!("Encountered invalid IP Type: {}", input);
                unreachable!()
            }
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct Ethernet {
    pub dest_mac: Mac,
    pub src_mac: Mac,
    pub vlan_header: Option<Vlan>,
    pub ether_type: EthernetType,
}

named!(
    pub ethernet<Ethernet>,
    do_parse!(
        dest_mac: mac
            >> src_mac: mac
            >> vlan_ether_type: dbg_dmp!(be_u16)
            >> vlan: dbg_dmp!(cond!(vlan_ether_type == 0x8100, vlan))
            >> orig_ether_type: cond!(vlan_ether_type == 0x8100, be_u16)
            >> ({
                let ether_type = if let Some(ether) = orig_ether_type {
                    EthernetType::from_u16(ether)
                } else {
                    EthernetType::from_u16(vlan_ether_type)
                };
                Ethernet {
                    dest_mac,
                    src_mac,
                    vlan_header: vlan,
                    ether_type,
                }
            })
    )
);

named!(
    vlan<Vlan>,
    do_parse!(
        bits: bits!(tuple!(
            take_bits!(u8, 3),
            take_bits!(u8, 1),
            take_bits!(u16, 12)
        )) >> (Vlan {
            pcp: bits.0,
            dei: bits.1,
            vlan_id: bits.2,
        })
    )
);

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Vlan {
    // tci: Vec<u8>,
    pcp: u8,
    dei: u8,
    vlan_id: u16,
}
